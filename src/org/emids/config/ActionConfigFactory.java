package org.emids.config;

import org.emids.biz.IAction;

public interface ActionConfigFactory {
	public IAction getAction(int actionId);
}
