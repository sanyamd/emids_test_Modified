package org.emids.config;

import org.emids.biz.GenericActionConstants;

public class PremiumActionConfig implements ActionConfig, GenericActionConstants {

	@Override
	public int nextAction(int actionId) {
		int nextActionId = 0;
		switch (actionId) {
		case CALCULATE_PREMIUM_AGE_RESULT:
			nextActionId =  CALCULATE_PREMIUM_GENDER;
			break;
		case CALCULATE_PREMIUM_BASE_AGE_RESULT:
			nextActionId =  COMPLETED;
			break;
		case CALCULATE_PREMIUM_GENDER_RESULT:
			nextActionId =  CALCULATE_PREMIUM_HEALTH_CONDITIONS;
			break;
		case CALCULATE_PREMIUM_HEALTH_CONDITIONS_RESULT:
			nextActionId =  CALCULATE_PREMIUM_HABBITS;
			break;
		case CALCULATE_PREMIUM_HABBITS_RESULT:
			nextActionId =  COMPLETED;
			break;
		default:
			break;
		}
		return nextActionId;
	}

	@Override
	public int firstAction() {
		// TODO Auto-generated method stub
		return CALCULATE_PREMIUM_AGE;
	}

}
