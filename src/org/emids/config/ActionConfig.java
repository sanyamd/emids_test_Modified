package org.emids.config;

public interface ActionConfig {
	public int nextAction(int actionId);
	public int firstAction();
}
