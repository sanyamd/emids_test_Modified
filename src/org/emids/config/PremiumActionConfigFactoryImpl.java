package org.emids.config;

import org.emids.biz.CalculatePremiumByAgeAction;
import org.emids.biz.CalculatePremiumOnGenderAction;
import org.emids.biz.CalculatePremiumOnHabbitsAction;
import org.emids.biz.CalculatePremiumOnHealthConditionsAction;
import org.emids.biz.GenericActionConstants;
import org.emids.biz.IAction;

public class PremiumActionConfigFactoryImpl implements ActionConfigFactory, GenericActionConstants {

	@Override
	public IAction getAction(int actionId) {
		IAction action = null;
		switch (actionId) {
		case CALCULATE_PREMIUM_AGE:
			action = new CalculatePremiumByAgeAction();
			break;

		case CALCULATE_PREMIUM_GENDER:
			action =  new CalculatePremiumOnGenderAction();
			break;
			
		case CALCULATE_PREMIUM_HABBITS:
			action =  new CalculatePremiumOnHabbitsAction();
			break;
			
		case CALCULATE_PREMIUM_HEALTH_CONDITIONS:
			action =  new CalculatePremiumOnHealthConditionsAction();
			break;
			
		default:
			break;
		}
		return action;
	}
	
}
