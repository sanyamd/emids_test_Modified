package org.emids.model;

import java.util.List;

public class Person {
	String name;
	char gender;
	int age;
	List<HealthConstants> healthConstants;
	List<Habbits> habbits;

	public Person(String name, char gender, int age) {
		this.name = name;
		this.gender = gender;
		this.age = age;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public char getGender() {
		return gender;
	}

	public void setGender(char gender) {
		this.gender = gender;
	}

	public List<HealthConstants> getHealthConstants() {
		return healthConstants;
	}

	public void setHealthConstants(List<HealthConstants> healthConstants) {
		this.healthConstants = healthConstants;
	}

	public List<Habbits> getHabbits() {
		return habbits;
	}

	public void setHabbits(List<Habbits> habbits) {
		this.habbits = habbits;
	}
	
	
}
