package org.emids.model;

public enum HealthConstants {
	HYPER_TENSION,
	BLOOD_PRESSURE,
	BLOOD_SUGAR,
	OVER_WEIGHT
}
