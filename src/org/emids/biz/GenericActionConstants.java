package org.emids.biz;

public interface GenericActionConstants {
	public static final int CALCULATE_PREMIUM_AGE = 1;
	public static final int CALCULATE_PREMIUM_AGE_RESULT = 2;
	public static final int CALCULATE_PREMIUM_GENDER = 3;
	public static final int CALCULATE_PREMIUM_GENDER_RESULT = 4;
	public static final int CALCULATE_PREMIUM_HEALTH_CONDITIONS = 5;
	public static final int CALCULATE_PREMIUM_HEALTH_CONDITIONS_RESULT = 6;
	public static final int CALCULATE_PREMIUM_HABBITS = 7;
	public static final int CALCULATE_PREMIUM_HABBITS_RESULT = 8;
	
	public static final int CALCULATE_PREMIUM_BASE_AGE_RESULT = 9999;
	public static final int COMPLETED = 100000;
}
