
package org.emids.biz;

import org.emids.config.ActionConfig;
import org.emids.config.ActionConfigFactory;
import org.emids.model.Person;

public class GenericActionController {
	
	ActionConfig actionConfig;
	ActionConfigFactory actionConfigFactory;
	
	private static double premium;
	static {
		premium = 5000;
	}
	
	
	public GenericActionController(ActionConfig actionConfig, ActionConfigFactory actionConfigFactory) {
		this.actionConfig = actionConfig;
		this.actionConfigFactory = actionConfigFactory;
	}
	
	public void start(Person person) {
		int actionId = actionConfig.firstAction();
		IAction action = actionConfigFactory.getAction(actionId);
		action.setController(this);
		
		while (true) {
			int resultId = action.execute(person);
			System.out.println(resultId);
			if (resultId == GenericActionConstants.COMPLETED 
					|| actionConfig.nextAction(resultId) == GenericActionConstants.COMPLETED) {
				break;
			} else {
				actionId = actionConfig.nextAction(resultId);
				action = actionConfigFactory.getAction(actionId);
				action.setController(this);
			}
		}
	}
	
	public double getPremium() {
		return this.premium;
	}
	
	public void setPremium(double premium) {
		this.premium = premium;
	}
}
