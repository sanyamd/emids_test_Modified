package org.emids.biz;

import org.emids.model.Habbits;
import org.emids.model.Person;

public class CalculatePremiumOnHabbitsAction implements IAction {
	
	GenericActionController controller;
	
	@Override
	public int execute(Person person) {
		double premiumAccumulatedOnHabbits = 0;
		for (Habbits habbit : person.getHabbits()) {
			if (habbit == Habbits.DAILY_EXCERCISE) {
				premiumAccumulatedOnHabbits -= ((premiumAccumulatedOnHabbits * 3) / 100);
			} else {
				premiumAccumulatedOnHabbits += ((premiumAccumulatedOnHabbits * 3) / 100);
			}
		}
		
		controller.setPremium(controller.getPremium()+premiumAccumulatedOnHabbits);
		return GenericActionConstants.CALCULATE_PREMIUM_HABBITS_RESULT;
	}

	@Override
	public void setController(GenericActionController controller) {
		this.controller = controller;
	}

}
