package org.emids.biz;

import java.util.ArrayList;
import java.util.List;

import org.emids.config.ActionConfig;
import org.emids.config.ActionConfigFactory;
import org.emids.config.PremiumActionConfig;
import org.emids.config.PremiumActionConfigFactoryImpl;
import org.emids.model.Habbits;
import org.emids.model.HealthConstants;
import org.emids.model.Person;

public class HealthInsurancePremiumCalculator {

	public static long calculateInsurancePremium(Person person) {
		ActionConfig actionConfig = new PremiumActionConfig();
		ActionConfigFactory actionConfigFactory = new PremiumActionConfigFactoryImpl();
		GenericActionController genericActionController = new GenericActionController(actionConfig, actionConfigFactory);
		
		genericActionController.start(person);
		return Math.round(genericActionController.getPremium());

	}

	public static void main(String[] args) {
		Person person = new Person("Norman Gomes", 'M', 34);
		List<HealthConstants> healthConstants = new ArrayList<>();
		healthConstants.add(HealthConstants.OVER_WEIGHT);
		
		List<Habbits> habbits = new ArrayList<>();
		habbits.add(Habbits.ALCHOHAL);
		habbits.add(Habbits.DAILY_EXCERCISE);

		person.setHabbits(habbits);
		person.setHealthConstants(healthConstants);

		System.out.println(calculateInsurancePremium(person));
	}
}
