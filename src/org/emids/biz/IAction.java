package org.emids.biz;

import org.emids.model.Person;

public interface IAction {
	public int execute(Person person);
	public void setController(GenericActionController controller);
}
