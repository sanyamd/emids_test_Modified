package org.emids.biz;

import org.emids.model.Person;

public class CalculatePremiumByAgeAction implements IAction {

	GenericActionController controller;

	@Override
	public int execute(Person person) {
		if (person.getAge() > 18) {
			double premiumAccumulatedOnAge = controller.getPremium();
			int[] ageSlabs = { 18, 25, 30, 35 };
			for (int ageSlab : ageSlabs) {
				if (person.getAge() > ageSlab) {
					premiumAccumulatedOnAge += ((premiumAccumulatedOnAge * 10) / 100);
				}
			}
			if (person.getAge() > 40) {
				premiumAccumulatedOnAge += ((premiumAccumulatedOnAge * 20) / 100);
			}
			controller.setPremium(premiumAccumulatedOnAge);
			return GenericActionConstants.CALCULATE_PREMIUM_AGE_RESULT;
		} else {
			return GenericActionConstants.CALCULATE_PREMIUM_BASE_AGE_RESULT;
		}
	}

	@Override
	public void setController(GenericActionController controller) {
		this.controller = controller;
	}

}
