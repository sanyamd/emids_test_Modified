package org.emids.biz;

import org.emids.model.Person;

public class CalculatePremiumOnGenderAction implements IAction {
	
	GenericActionController controller;
	
	@Override
	public int execute(Person person) {
		double premiumAccumulatedOnGender = controller.getPremium();
		if (person.getGender() == 'M' || person.getGender() == 'm') {
			premiumAccumulatedOnGender += ((premiumAccumulatedOnGender * 2) / 100);
		}
		
		controller.setPremium(premiumAccumulatedOnGender);
		return GenericActionConstants.CALCULATE_PREMIUM_GENDER_RESULT;
	}

	@Override
	public void setController(GenericActionController controller) {
		this.controller = controller;
	}

}
