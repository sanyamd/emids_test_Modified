package org.emids.biz;

import org.emids.model.HealthConstants;
import org.emids.model.Person;

public class CalculatePremiumOnHealthConditionsAction implements IAction {
	
	GenericActionController controller;
	
	@Override
	public int execute(Person person) {
		double premiumAccumulatedOnHealthCondition = controller.getPremium();
		for (HealthConstants healthConstant : person.getHealthConstants()) {
			premiumAccumulatedOnHealthCondition += ((premiumAccumulatedOnHealthCondition * 1) / 100); 
		}
		controller.setPremium(premiumAccumulatedOnHealthCondition);
		return GenericActionConstants.CALCULATE_PREMIUM_HEALTH_CONDITIONS_RESULT;
	}

	@Override
	public void setController(GenericActionController controller) {
		this.controller = controller;
	}

}
