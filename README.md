
<h1> README </h1>

Its a sample health insurance premium calculator.

Made using <b>Chain of responsibilities pattern<b>.

<b>Controller</b>
GenericActionController - Controller which is starting the <b>chain of actions</b>.
IAction - Interface for various Actions
<h6>SubClasses - CalculatePremiumByAgeAction, CalculatePremiumOnGenderAction, CalculatePremiumOnHabbitsAction, CalculatePremiumOnHealthConditionsAction</h6>

<b>Configuration</b>
ActionConfig - Interface for configuring the action chain
ActionConfigFactory - Interface for configuring the action objects for specific actions 

<b>Model</b>
Person[emids_test/src/org/emids/model] - Model class for Person who wants to query for premium.
HealthConstants & Habbits [emids_test/src/org/emids/model] - 2 Enums for Person Habbits and Health Conditions


src/TestClass.java - Java test class to test the app using console

HealthInsurancePremiumCalculator [emids_test/src/org/emids/biz]- Test Class

<b> JUnit </b>
HealthInsurancePremiumCalculatorTest [emids_test/test/org/emids/biz]- sample 2 short Test cases written using Junit4


